import 'package:flutter/material.dart';
import 'package:share/share.dart';

class GifPage extends StatelessWidget {
  Map _gifData;
  String link;

  GifPage(gifData) {
    this._gifData = gifData;
    this.link = _gifData["images"]["fixed_height"]["url"];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_gifData["title"]),
        backgroundColor: Colors.black12,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.share), onPressed: () {
            Share.share(link);
          })
        ],
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Center(
          child: Image.network(_gifData["images"]["fixed_height"]["url"]),
        ),
      ),
    );
  }
}
